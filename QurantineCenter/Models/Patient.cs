﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QurantineCenter.Models
{
    public class Patient
    {

        public int PatientId { get; set; }
        public string PatientName { get; set; }
        public int PatientAge { get; set; }
        public int CenterId { get; set; }
        public virtual Center Center { get; set; }
    }
}
